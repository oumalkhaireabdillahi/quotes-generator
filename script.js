const quoteContainer = document.getElementById('quote-container');
const quoteText = document.getElementById('quote'); 
const authorText = document.getElementById('author');
const twitterBtn = document.getElementById('twitter');
const newQuoteBtn = document.getElementById('new-quote');
const loader = document.getElementById('loader');

let apiQuotes = [];

// Show Loading

function loading() {
   loader.hidden.style.display = '';
   quoteContainer.style.display = 'none';

   
}
// Hide Loading

function complete() {
    quoteContainer.hidden = 'block';
    loader.hidden = 'none';
}



// Show New Quotes

function newQuote() {

   //loading();

    // Pick a Random Quote from apiQuotes array
    const quote = apiQuotes[Math.floor(Math.random()* apiQuotes.length)];
    
    //Check if Author field is blank and replace it with 'Unknown'

    if (!quote.author)
 {
     authorText.textContent= 'Unknown'
 }  else {
    authorText.textContent = quote.author;
 }
 // Check Quote length to determine styling

 if (quote.text.length > 120) {
     quoteText.classList.add('long-quote');
 } else {
     quoteText.classList.remove('long-quote');
 }
    // Set Quote, Hide Loader 
    quoteText.textContent = quote.text;
// complete();
}



// Get Quotes From API

async function getQuotes () {

    // const proxyURL = 'https://cors-anywhere.herokuapp.com/'
// we we use the free proxy api from CORS anywhere. It often results in http status code 429, because there is too much traffic.

    const apiURL = 'https://type.fit/api/quotes';
    try {
    //  const response = await fetch(proxyURL + apiURL);
        const response = await fetch(apiURL);
        apiQuotes = await response.json();
        newQuote();
    } catch (error) {
        
    }
}

// Tweet Quote

function tweetQuote() {
const twitterUrl = `https://twitter.com/intent/tweet?text=${quoteText.textContent} - ${authorText.textContent}`;
window.open(twitterUrl, '_blank');
}

// Event Listeners

newQuoteBtn.addEventListener('click', newQuote);
twitterBtn.addEventListener('click', tweetQuote);

// On Load

getQuotes();
//loading();